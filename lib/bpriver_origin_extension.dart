// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library bpriver_origin_extension;

import 'dart:math';

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:change_case/change_case.dart';

part 'src/bpriver_origin_extension_exception/bpriver_origin_extension_exception.dart';

part 'src/design_pattern_extension/iterable/iterable_pattern_not_empty.dart';
part 'src/design_pattern_extension/iterable/iterable_pattern_length_limit.dart';

part 'src/design_pattern_extension/iterable/iterable_pattern_internal_factory.dart';

part 'src/design_pattern_extension/iterable/list/list_pattern_add.dart';
part 'src/design_pattern_extension/iterable/list/list_pattern_add_all.dart';
part 'src/design_pattern_extension/iterable/list/list_pattern_plus_operator.dart';
part 'src/design_pattern_extension/iterable/list/list_pattern_reversed.dart';

part 'src/design_pattern_extension/iterable/roster/roster_pattern_put.dart';
part 'src/design_pattern_extension/iterable/roster/roster_pattern_put_all.dart';

part 'src/design_pattern_extension/value/num/between_x_and_y/between_0_and_10.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_0_and_100.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_0_and_1000.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_0_and_10000.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_0_and_20000.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_0_and_5000.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_1_and_10.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_minus_100_and_100.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_x_and_y.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_x_and_y_exception.dart';
part 'src/design_pattern_extension/value/num/between_x_and_y/between_x_and_y_operator.dart';

part 'src/design_pattern_extension/value/num/lower_limit/lower_limit.dart';

part 'src/design_pattern_extension/value/num/iterable_num_value_pattern.dart';
part 'src/design_pattern_extension/value/num/iterable_num_value_pattern_sum.dart';
part 'src/design_pattern_extension/value/num/num_value_pattern.dart';
part 'src/design_pattern_extension/value/num/num_value_pattern_get_em.dart';
part 'src/design_pattern_extension/value/num/num_value_pattern_get_ex.dart';
part 'src/design_pattern_extension/value/num/num_value_pattern_get_fr.dart';
part 'src/design_pattern_extension/value/num/num_value_pattern_get_percent.dart';
part 'src/design_pattern_extension/value/num/num_value_pattern_get_px.dart';
part 'src/design_pattern_extension/value/num/num_value_pattern_than_operator.dart';

part 'src/design_pattern_extension/value/only_one_character/only_one_character.dart';

part 'src/design_pattern_extension/value/string/string_value_pattern.dart';
part 'src/design_pattern_extension/value/string/string_value_pattern_is_empty.dart';
part 'src/design_pattern_extension/value/string/string_value_pattern_is_not_empty.dart';
part 'src/design_pattern_extension/value/string/allow_character_set/allow_character_set.dart';
part 'src/design_pattern_extension/value/string/allow_character_set/allow_character_set_exception.dart';
part 'src/design_pattern_extension/value/string/allow_set/allow_set.dart';
part 'src/design_pattern_extension/value/string/allow_set/allow_set_exception.dart';
part 'src/design_pattern_extension/value/string/allow_starting_character_set/allow_starting_character_set.dart';
part 'src/design_pattern_extension/value/string/allow_starting_character_set/allow_starting_character_set_exception.dart';
part 'src/design_pattern_extension/value/string/forbid_character_set/forbid_character_set.dart';
part 'src/design_pattern_extension/value/string/forbid_character_set/forbid_character_set_exception.dart';
part 'src/design_pattern_extension/value/string/forbid_set/forbid_set.dart';
part 'src/design_pattern_extension/value/string/forbid_starting_character_set/forbid_starting_character_set.dart';
part 'src/design_pattern_extension/value/string/forbid_starting_character_set/forbid_starting_character_set_exception.dart';
part 'src/design_pattern_extension/value/string/identifier/identifier.dart';
part 'src/design_pattern_extension/value/string/identifier/identifier_exception.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet_camel_case.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet_constant_case.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet_end.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet_exception.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet_pascal_case.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet_snake_case.dart';
part 'src/design_pattern_extension/value/string/lower_case_alphabet/lower_case_alphabet_type_annotation_case.dart';
part 'src/design_pattern_extension/value/string/not_empty/not_empty.dart';

part 'src/design_pattern_extension/value/type/type_value_pattern.dart';
part 'src/design_pattern_extension/value/type/type_value_pattern_allow_set.dart';
part 'src/design_pattern_extension/value/type/type_value_pattern_allow_set_exception.dart';
