// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template RosterPatternPutAll}
/// 
/// PK...primary key
/// VT...vlaue type
/// RT... return type
/// 
/// value を追加 もしくは 上書き する.<br>
/// 
/// [putAll] によって [RosterPatternExceptionA] は発生しない.<br>
/// なので 呼び出し元は これを throw out する必要がある.<br>
/// 
/// {@endtemplate}
mixin RosterPatternPutAll<
        PK extends Object
        ,VT extends EntityPattern<PK>
        ,RT extends Result<Iterable<VT>>
    >
    on
        RosterPattern<PK, VT>
        ,IterablePatternInternalFactory<VT,RT>
{

    /// {@macro RosterPatternPutAll}
    RT putAll(Iterable<VT> putValues) {

        final List<VT> list = [];

        // values の中に putValues と同じものがあれば putValues と差し替える.
        for (final value in values) {

            bool flag = true;

            for (final putValue in putValues) {
                if (value.primaryKey == putValue.primaryKey) {
                    list.add(putValue);
                    flag = false;   
                }
            }

            if (flag) list.add(value);

        }

        // values の 中に存在しない putValues を追加する.
        for (final putValue in putValues) {
            
            final containsPrimaryKeyResult = containsPrimaryKey(putValue.primaryKey);

            if (!containsPrimaryKeyResult.wrapped) list.add(putValue);

        }

        return internalFactory(list);

    }

}
