// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template IterablePatternLengthLimit}
/// 
/// VT・・・ValueType.
/// 
/// Throw wrapped [IterablePatternLengthLimitException] by [Failure] then 指定した長さを超過.
/// 
/// {@endtemplate}
mixin IterablePatternLengthLimit<
        VT extends Object
    >
    on
        IterablePattern<VT>
{

    int get lengthLimit;

    /// {@macro IterablePatternLengthLimit}
    void checkIterablePatternLengthLimit() {

        final log = Log(classLocation: runtimeType, functionLocation: 'checkIterablePatternLengthLimit');

        if(values.length <= lengthLimit) return;

        log.monitorAll({
            'length limit': lengthLimit,
            'length': length,
        });

        throw Failure(IterablePatternLengthLimitException(), log);

    }

}
