// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template ListPatternReversed}
/// 
/// * VT...ValuesType
/// * RT...ReturnType
/// 
/// 逆の順序で返す。
/// 
/// {@endtemplate}
// mixin ListPatternReversed<
//         VT extends Object
//         ,RT
//     >
//     on
//         ListPattern<VT>
//         ,IterablePatternInternalFactory<VT,RT>
// {

//     RT get reversed {
//         return internalFactory(values.toList().reversed);
//     }

// }