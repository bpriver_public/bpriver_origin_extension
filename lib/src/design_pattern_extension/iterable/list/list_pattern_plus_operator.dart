// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template ListPatternPlusOperator}
/// 
/// * VT...ValuesType
/// * RT...ReturnType
/// 
/// value を追加。
/// 
/// {@endtemplate}
// mixin ListPatternPlusOperator<
//         VT extends Object
//         ,RT
//     >
//     on
//         ListPattern<VT>
//         ,ListPatternAddAll<VT,RT>
// {

//     RT operator +(Iterable<VT> values) => addAll(values);

// }