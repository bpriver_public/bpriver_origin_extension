// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template IterablePatternNotEmpty}
/// 
/// VT・・・ValueType.
/// 
/// Throw wrapped [IterablePatternNotEmptyException] by [Failure] then [values] is empty.
/// 
/// {@endtemplate}
mixin IterablePatternNotEmpty<
        VT extends Object
    >
    on
        IterablePattern<VT>
{

    /// {@macro IterablePatternNotEmpty}
    void checkIterablePatternNotEmpty() {

        final log = Log(classLocation: IterablePatternNotEmpty<VT>, functionLocation: 'checkIterablePatternNotEmpty');

        if(values.isEmpty) throw Failure(IterablePatternNotEmptyException(), log);

    }

}
