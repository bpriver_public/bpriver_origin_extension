// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template ForbidSet}
/// 
/// Throw wrapped [ForbidSetException] by [Failure] then 列挙した 文字列 以外が指定(※空文字は許容する).
/// 
/// {@endtemplate}
mixin ForbidSet
    on
        StringValuePattern
{

    Set<String> get forbidSet;

    /// {@macro ForbidSet}
    static void check(String value, Set<String> forbidSet) {

        final log = Log(classLocation: ForbidSet, functionLocation: 'check');

        if (value.isEmpty) return;

        final containsResult = forbidSet.contains(value);

        if (containsResult) {
            log.monitor('value', value);
            log.monitor('forbid set', forbidSet);
            throw Failure(ForbidSetException(), log);
        }

    }

    /// {@macro ForbidSet}
    void checkForbidSet() => check(value, forbidSet);
    
}
