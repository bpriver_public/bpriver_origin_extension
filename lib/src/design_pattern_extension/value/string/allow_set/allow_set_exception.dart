// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template AllowSetException}
/// {@endtemplate}
sealed class AllowSetException
    extends
        BpriverOriginExtensionException
{}

/// {@template AllowSetExceptionA}
/// 列挙した 文字列 以外が指定された.
/// {@endtemplate}
class AllowSetExceptionA
    extends
        AllowSetException
{

    static void check(String value, Set<String> allowSet) {
        if (value.isEmpty) return;
        if (allowSet.contains(value)) return;
        throw AllowSetExceptionA(value ,allowSet);
    }

    @override
    final List<String> loggerResultMessage;

    AllowSetExceptionA(String value, Set<String> allowSet)
    :
        loggerResultMessage = [
            '列挙した 文字列 以外が指定された.',
            'value に入る値は ${allowSet} のいずれかでなければいけません.',
            '指定された value の値 = ${value}',
        ]
    ;

}
