// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template AllowStartingCharacterSetException}
/// {@endtemplate}
sealed class AllowStartingCharacterSetException
    extends
        BpriverOriginExtensionException
{}

/// {@template AllowStartingCharacterSetExceptionA}
/// 列挙した 文字 以外で開始.
/// 空文字は許容する.
/// {@endtemplate}
class AllowStartingCharacterSetExceptionA
    extends
        AllowStartingCharacterSetException
{

    static void check(String value, Set<String> allowStartingCharacterSet) {
        
        if (value.isEmpty) return;

        for (final character in allowStartingCharacterSet) {
            
            final startsWithResult = value.startsWith(character);

            if (startsWithResult) return;

        }

        throw AllowStartingCharacterSetExceptionA(value, allowStartingCharacterSet);

    }

    @override
    final List<String> loggerResultMessage;

    AllowStartingCharacterSetExceptionA(String value, Set<String> allowStartingCharacterSet)
    :
        loggerResultMessage = [
            '列挙した 文字 以外で開始.',
            '${allowStartingCharacterSet} の文字で開始してください.',
            'value の値 = ${value}',
        ]
    ;

}
