// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template NotEmpty}
/// 
/// Throw wrapped [NotEmptyException] by [Failure] then [value] is empty.
/// 
/// {@endtemplate}
mixin NotEmpty
    implements
        ValuePattern<String>
{

    /// {@macro NotEmpty}
    static void check(String value) {

        final log = Log(classLocation: NotEmpty, functionLocation: 'checkNotEmpty');

        if(value.isNotEmpty) return;
        
        throw Failure(NotEmptyException(), log.monitorAll({
            'input value': value,
        }));

    }

    /// {@macro NotEmpty}
    void checkNotEmpty() => check(value);

}
