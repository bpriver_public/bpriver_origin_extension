// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template AllowCharacterSetException}
/// {@endtemplate}
sealed class AllowCharacterSetException
    extends
        BpriverOriginExtensionException
{}

/// {@template AllowCharacterSetExceptionA}
/// 許可されていない文字の利用.
/// {@endtemplate}
class AllowCharacterSetExceptionA
    extends
        AllowCharacterSetException
{

    static void check(String value, Set<String> allowCharacterSet) {
        if (value.isEmpty) return;
        final targetCharacterSet = value.split('');
        for (final target in targetCharacterSet) {
            if (allowCharacterSet.contains(target)) continue;
            throw AllowCharacterSetExceptionA(value, allowCharacterSet);
        }
    }

    @override
    final List<String> loggerResultMessage;

    AllowCharacterSetExceptionA(String value, Set<String> allowCharacterSet)
    :
        loggerResultMessage = [
            '許可されていない文字の利用.',
            'value に入る値は ${allowCharacterSet} の文字しか利用できません',
            '指定された value の値 = ${value}',
        ]
    ;
    
}
