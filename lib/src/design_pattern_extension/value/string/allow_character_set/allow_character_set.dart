// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// 
/// 列挙した 文字 以外が検出された場合 error.
/// empty は許容する.
/// 
mixin AllowCharacterSet
    on
        ValuePattern<String>
{

    Set<String> get allowCharacterSet;

    void checkAllowCharacterSet() {
        AllowCharacterSetExceptionA.check(value, allowCharacterSet);
    }
    
}
