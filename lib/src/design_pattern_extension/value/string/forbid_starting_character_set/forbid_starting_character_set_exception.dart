// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template ForbidStartingCharacterSetException}
/// {@endtemplate}
sealed class ForbidStartingCharacterSetException
    extends
        BpriverOriginExtensionException
{}

/// {@template ForbidStartingCharacterSetExceptionA}
/// 列挙した 文字 で開始.
/// 空文字は許容する.
/// {@endtemplate}
class ForbidStartingCharacterSetExceptionA
    extends
        ForbidStartingCharacterSetException
{

    static void check(String value, Set<String> forbidStartingCharacterSet) {
        
        if (value.isEmpty) return;

        for (final forbidStartingCharacter in forbidStartingCharacterSet) {

            final startWithResult = value.startsWith(forbidStartingCharacter);
        
            if (startWithResult) throw ForbidStartingCharacterSetExceptionA(value, forbidStartingCharacterSet);
        
        }

    }

    @override
    final List<String> loggerResultMessage;

    ForbidStartingCharacterSetExceptionA(String value, Set<String> forbidStartingCharacterSet)
    :
        loggerResultMessage = [
            '列挙した 文字 で開始.',
            '${forbidStartingCharacterSet} 以外の文字で開始してください.',
            'value の値 = ${value}',
        ]
    ;

}
