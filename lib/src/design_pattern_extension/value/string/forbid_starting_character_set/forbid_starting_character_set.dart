// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template ForbidStartingCharacterSet}
/// 列挙した 文字列 以外で開始されたら throw exception.
/// 空文字は許容する.
/// {@endtemplate}
mixin ForbidStartingCharacterSet
    on
        StringValuePattern
{

    Set<String> get forbidStartingCharacterSet;

    void checkForbidStartingCharacterSet() {
        ForbidStartingCharacterSetExceptionA.check(value, forbidStartingCharacterSet);
    }
    
}
