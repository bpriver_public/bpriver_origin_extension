// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// 
/// type annotation は class 名と同じというだけですべてが pascal case というわけではない.
/// それは num, int, double, bool, void, dynamic, がそうである.
/// 上記の場合はそのままで それ以外は pascal case で返す.
/// 
mixin LowerCaseAlphabetTypeAnnotationCase
    implements
        LowerCaseAlphabet
        ,LowerCaseAlphabetPascalCase
{

    static const Set<String> values = {
        int,
        double,
        num,
        bool,
        dynamic,
        void$,
    };

    // これらは pascal case ではない BuiltInType である.
    static const String num = 'num';
    static const String int = 'int';
    static const String double = 'double';
    static const String bool = 'bool';
    static const String void$ = 'void';
    static const String dynamic = 'dynamic';

    String get typeAnnotationCase {
        if(value == num) return value;
        if(value == int) return value;
        if(value == double) return value;
        if(value == bool) return value;
        if(value == void$) return value;
        if(value == dynamic) return value;
        return pascalCase;
    }

}