// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

sealed class LowerCaseAlphabetException
    extends
        BpriverOriginExtensionException
{}

/// {@template LowerCaseAlphabetException}A
/// value が lower case じゃない.
/// {@endtemplate}
class LowerCaseAlphabetExceptionA
    extends
        LowerCaseAlphabetException
{

    static void check(String value) {
        if(value.isEmpty) return;
        /// isLowerCase() は 空文字だと false を返す
        /// が 空文字は許容したい
        if(value.isLowerCase()) return;
        throw LowerCaseAlphabetExceptionA(value);
    }

    @override
    final List<String> loggerResultMessage;

    LowerCaseAlphabetExceptionA(String value)
    :
        loggerResultMessage = [
            'lower case である必要があります。',
            '入力した value = $value',
        ]
     ;

}
