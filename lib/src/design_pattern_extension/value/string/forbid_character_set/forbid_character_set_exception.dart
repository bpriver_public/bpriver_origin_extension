// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template ForbidCharacterSetException}
/// {@endtemplate}
sealed class ForbidCharacterSetException
    extends
        BpriverOriginExtensionException
{}

/// {@template ForbidCharacterSetExceptionA}
/// 列挙した 文字 を利用.
/// 空文字は許容する.
/// {@endtemplate}
class ForbidCharacterSetExceptionA
    extends
        ForbidCharacterSetException
{

    static void check(String value, Set<String> forbidCharacterSet) {
        
        if (value.isEmpty) return;

        for (final forbidCharacter in forbidCharacterSet) {
            
            final containsResult = value.contains(forbidCharacter);
            
            if (containsResult) throw ForbidCharacterSetExceptionA(value, forbidCharacterSet);

        }

    }

    @override
    final List<String> loggerResultMessage;

    ForbidCharacterSetExceptionA(String value, Set<String> forbidCharacterSet)
    :
        loggerResultMessage = [
            '列挙した 文字 を利用.',
            '${forbidCharacterSet} の文字は使用しないでください.',
            'value の値 = ${value}',
        ]
    ;

}
