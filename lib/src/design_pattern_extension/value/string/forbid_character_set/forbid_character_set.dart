// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template ForbidCharacterSet}
/// 列挙した 文字 以外で開始されたら throw exception.
/// 指定できる文字は １文字でなくてもよい.
///     つまり ==, abc, などと 指定できる.
///     個のばあい =, a, b, c, は利用でき, ==, abc, となっている箇所があれば throw excetpion.
/// 空文字は許容する.
/// {@endtemplate}
mixin ForbidCharacterSet
    on
        StringValuePattern
{

    Set<String> get forbidCharacterSet;

    void checkForbidCharacterSet() {
        ForbidCharacterSetExceptionA.check(value, forbidCharacterSet);
    }
    
}
