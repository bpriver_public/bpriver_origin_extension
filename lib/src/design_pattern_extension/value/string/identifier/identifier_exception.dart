// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template IdentifierException}
/// {@endtemplate}
sealed class IdentifierException
    extends
        BpriverOriginExtensionException
{}

/// {@template IdentifierExceptionA}
/// 文字数が 0 文字 または 21 文字 ではない.
/// {@endtemplate}
class IdentifierExceptionA
    extends
        IdentifierException
{

    static void check(String value) {
        
        final length = value.length;

        // empty を許容
        if (length == 0) return;

        if(length == Identifier.digit) return;

        throw IdentifierExceptionA(value);

    }

    @override
    final List<String> loggerResultMessage;

    IdentifierExceptionA(String value)
    :
        loggerResultMessage = [
            '文字数が 0 文字 または 21 文字 ではない.'
            '入力された identifier の値 = 「$value」',
            'identifier に利用できる文字数は 「${Identifier.digit}」 文字です。',
            '入力された文字数は 「${value.length}」 でした。',
        ]
     ;

}

/// {@template IdentifierExceptionB}
/// used invalid letter.
/// {@endtemplate}
class IdentifierExceptionB
    extends
        IdentifierException
{

    static void check(String value) {
        // empty の場合 rebExp の時点で null error が発生.
        if (value.length == 0) return;
        if(Identifier.material_regExp.firstMatch(value)!.group(0)!.length == Identifier.digit) return;
        throw IdentifierExceptionB(value);
    }

    @override
    final List<String> loggerResultMessage;

    IdentifierExceptionB(String value)
    :
        loggerResultMessage = [
            'used invalid letter.'
            '入力された identifier の値 = 「$value」',
            'identifier に利用できる文字は 「${Identifier.material}」 です。',
        ]
    ;

}
