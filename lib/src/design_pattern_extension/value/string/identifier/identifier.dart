// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// empty は許容し その場合 id が設定されていないものと見なす.
mixin Identifier
    implements
        ValuePattern<String>
{

    static const material='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    //利用可能桁数
    static const digit=21;
    static const inactivateValue = 'xxxxxxxxxxxxxxxxxxxxx';

    static final material_regExp= RegExp(r'(\w+)');

    void checkIdentifier(){
        IdentifierExceptionA.check(value);
        IdentifierExceptionB.check(value);
    }


    @override
    String inactivate() {
        return inactivateValue;
    }

    static String generateID(){
        var id = '';
        var randomNumberRange = Identifier.material.length - 1; // 62 - 1 = 61

        for(var i = 0 ; i < Identifier.digit ; i++){
            var randomNumber = Random().nextInt(randomNumberRange); // 0 ~ 61 がランダムで生成される
            id +=  Identifier.material.substring(randomNumber,randomNumber + 1);
        }
        return id;
    }

}