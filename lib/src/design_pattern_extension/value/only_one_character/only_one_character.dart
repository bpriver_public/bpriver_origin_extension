// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template OnlyOneCharacter}
/// 
/// Throw wrapped [OnlyOneCharacterException] by [Failure] then 文字数が1以外(空文字も受け付けない).
/// 
/// {@endtemplate}
mixin OnlyOneCharacter<
        VT extends Object
    >
    on
        ValuePattern<VT>
{

    /// {@macro OnlyOneCharacter}
    static void check(Object value) {

        final log = Log(classLocation: OnlyOneCharacter, functionLocation: 'check');

        final length = value.toString().length;

        if (length == 1) return;

        log.monitor('value', value);
        log.monitor('value length', value.toString().length);
        throw Failure(OnlyOneCharacterException(), log);

    }

    /// {@macro OnlyOneCharacter}
    void checkOnlyOneCharacter() => check(value);

}
