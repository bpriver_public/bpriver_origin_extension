// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

sealed class TypeValuePatternAllowSetException
    extends
        BpriverOriginExtensionException
{}

/// 列挙した Type 以外を指定された.
class TypeValuePatternAllowSetExceptionA
    extends
        TypeValuePatternAllowSetException
{

    static void check(Type value, Set<Type> allowSet) {
        if (allowSet.contains(value)) return;
        throw TypeValuePatternAllowSetExceptionA(value, allowSet);
    }

    @override
    final List<String> loggerResultMessage;

    TypeValuePatternAllowSetExceptionA(Type value, Set<Type> allowTypeSet)
    :
        loggerResultMessage = [
            'value に入る値は ${allowTypeSet} のいずれかでなければいけません.',
            '指定された value の値 = ${value}',
        ]
    ;

}