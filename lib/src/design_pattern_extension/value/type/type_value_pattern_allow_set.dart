// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// 
/// 列挙した Type 以外を指定された場合 error.
/// 
/// allow list を with した class は get allowSet を override して 許可する set を返すようにする
/// 
mixin TypeValuePatternAllowSet
    on
        TypeValuePattern
{

    Set<Type> get allowSet;

    void checkAllowSet() {
        TypeValuePatternAllowSetExceptionA.check(value, allowSet);
    }
    
}
