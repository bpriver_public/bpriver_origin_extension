// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.


// TODO 設計が荒い。test 書いてない。
part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// 
/// VT...ValueType
/// MS...Myself
/// 
/// 合計を返す。
/// 
mixin IterableNumValuePatternSum<
        VT extends ValuePattern<num>
        ,MS extends IterableNumValuePatternSum<VT,MS>
    >
    implements
        IterableNumValuePattern<MS>
{

    num sum() {
        num total = 0;
        /// どの value も lowerLimit,upperLimit ともに、同一なので、ok.
        /// また、空の values も無い。
        // final lowerLimit = values.first.lowerLimit;
        // final upperLimit = values.first.upperLimit;
        forEach((element) {
            total = total + element.value;
        });
        // for (final e in values) {
            
        // }
        // if(total < lowerLimit) return values.first.internalFactory(lowerLimit as V);
        // if(total > upperLimit) return values.first.internalFactory(upperLimit as V);
        return total;
    }
    
}
