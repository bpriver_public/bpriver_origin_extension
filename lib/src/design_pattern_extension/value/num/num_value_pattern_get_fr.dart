// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// Fr => Fraction
mixin NumValuePatternGetFr<
        VT extends num
    >
    implements
        NumValuePattern<VT>
{
    
    String get fr => value.toString() + 'fr';

}