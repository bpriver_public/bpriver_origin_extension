// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.
part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template LowerLimit}
/// 
/// VT ... ValueType
/// 
/// Throw wrapped [LowerLimitException] by [Failure] then value is too small.
/// 
/// {@endtemplate}
mixin LowerLimit<
        VT extends num
    >
    on
        NumValuePattern<VT>
{
    
    num get lowerLimit;

    /// {@macro LowerLimit}
    static void check(num value, num lowerLimit) {

        final log = Log(classLocation: LowerLimit, functionLocation: 'check');

        if(value < lowerLimit) {
            log.monitor('value', value);
            log.monitor('lower limit', lowerLimit);
            throw Failure(LowerLimitException(), log);
        }

    }

    /// {@macro LowerLimit}
    void checkLowerLimit() => check(value, lowerLimit);

}
