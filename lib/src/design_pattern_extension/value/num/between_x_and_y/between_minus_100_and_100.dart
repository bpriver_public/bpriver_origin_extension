// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// 
/// V ... Value value に利用する型を指し示す。
/// MS ... Myself
/// M ... Module. 属している Module の型を指定する。
/// 
/// -100 から 100 までの数。
/// 
mixin BetweenMinus100And100<
        V extends num
    >
    on
        BetweenXAndY<V>
{
    
    @override
    num get lowerLimit => -100;
    
    @override
    num get upperLimit => 100;

}
