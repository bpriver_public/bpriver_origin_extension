// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template BetweenXAndYException}
/// {@endtemplate}
sealed class BetweenXAndYException
    extends
        BpriverOriginExtensionException
{}

/// {@template BetweenXAndYExceptionA}
/// inputed value is too small.
/// {@endtemplate}
class BetweenXAndYExceptionA
    extends
        BetweenXAndYException
{

    static void check(num value, num lowerLimit, num upperLimit, Type valueType) {
        if(value < lowerLimit) throw BetweenXAndYExceptionA(value, lowerLimit, upperLimit, valueType);
    }

    @override
    final List<String> loggerResultMessage;

    BetweenXAndYExceptionA(num value, num lowerLimit, num upperLimit, Type valueType)
    :
        loggerResultMessage = [
            'inputed value is too small.',
            '入力された値 = 「${value}」',
            '値が小さすぎます。',
            '利用できるのは、「${lowerLimit} ~ ${upperLimit}」の、${valueType} 型の値です。',
        ]
    ;

}

/// {@template BetweenXAndYExceptionB}
/// inputed value is too large.
/// {@endtemplate}
class BetweenXAndYExceptionB
    extends
        BetweenXAndYException
{

    static void check(num value, num lowerLimit, num upperLimit, Type valueType) {
        if(value > upperLimit) throw BetweenXAndYExceptionB(value, lowerLimit, upperLimit, valueType);
    }

    @override
    final List<String> loggerResultMessage;

    BetweenXAndYExceptionB(num value, num lowerLimit, num upperLimit, Type valueType)
    :
        loggerResultMessage = [
            'inputed value is too large.',
            '入力された値 = 「${value}」',
            '値が大きすぎます。',
            '利用できるのは、「${lowerLimit} ~ ${upperLimit}」の、${valueType} 型の値です。',
        ]
    ;

}