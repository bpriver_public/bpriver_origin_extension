// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

// 
// 引数と戻り値の型は両方ともvalue の型。
// 引数と戻り値の型を別々のものにすると複雑になる。
// それをしたければ、子 class で個別に実装すればよい。
// 基本的な operator で、色んな型を計算できるようにすると、それを把握するのが大変。
// 
/// 
/// VT...ValueType.
/// MS...MySelf.
/// 
/// 演算後、制限を超過した場合、制限の限界の値の M を返す。
/// 
mixin BetweenXAndYOperator
    <
        VT extends num
        ,MS extends BetweenXAndY<VT>
    >
    on
        BetweenXAndY<VT>
{

    MS internalFactory(VT value);
    
    MS operator +(MS other) {
        final result = value + other.value;
        if(result < lowerLimit) return internalFactory(lowerLimit as VT);
        if(result > upperLimit) return internalFactory(upperLimit as VT);
        return internalFactory(result as VT);
    }

    MS operator -(MS other) {
        final result = value - other.value;
        if(result < lowerLimit) return internalFactory(lowerLimit as VT);
        if(result > upperLimit) return internalFactory(upperLimit as VT);
        return internalFactory(result as VT);
    }

    MS add(VT other) {
        return this + internalFactory(other);
    }

    MS subtract(VT other) {
        return this - internalFactory(other);
    }

}