// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// 
/// V ... Value value に利用する型を指し示す。
/// MS ... Myself
/// M ... Module. 属している Module の型を指定する。
/// 
mixin BetweenXAndY<
        V extends num
        // ,MS extends BetweenXAndY<V,MS>
    >
    implements
        ValuePattern<V>
{
    
    /// 
    /// この値が、取りうる値の下限となる。
    /// 
    num get lowerLimit;

    /// 
    /// この値が、取りうる値の上限となる。
    /// 
    num get upperLimit;

    void checkBetweenXAndY() {
        BetweenXAndYExceptionA.check(value, lowerLimit, upperLimit, V);
        BetweenXAndYExceptionB.check(value, lowerLimit, upperLimit, V);
    }

}
