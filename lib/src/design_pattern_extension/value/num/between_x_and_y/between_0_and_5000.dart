// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// 
/// V ... Value value に利用する型を指し示す。
/// MS ... Myself
/// M ... Module. 属している Module の型を指定する。
/// 
/// 0 から 5000 までの数。
/// 
mixin Between0And5000<
        V extends num
    >
    on
        BetweenXAndY<V>
{
    
    @override
    num get lowerLimit => 0;
    
    @override
    num get upperLimit => 5000;

}
