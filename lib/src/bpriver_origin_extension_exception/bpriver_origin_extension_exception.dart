// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

/// {@template BpriverOriginExtensionException}
/// {@endtemplate}
sealed class BpriverOriginExtensionException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const BpriverOriginExtensionException();

    @override
    Map<String, Object> toMap() {
        return {
            'loggerResultMessage': loggerResultMessage,
        };
    }

}

/// {@template IterablePatternNotEmptyException}
/// old BpriverOriginExtensionExceptionA.<br>
/// values is empty.
/// {@endtemplate}
class IterablePatternNotEmptyException
    extends
        BpriverOriginExtensionException
{

    static const LOGGER_RESULT_MESSAGE = [
        'values is empty.',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro IterablePatternNotEmptyException}
    IterablePatternNotEmptyException();

}

/// {@template IterablePatternLengthLimitException}
/// old BpriverOriginExtensionExceptionB.
/// 指定した長さを超過.
/// {@endtemplate}
class IterablePatternLengthLimitException
    extends
        BpriverOriginExtensionException
{

    static const LOGGER_RESULT_MESSAGE = [
        '指定した長さを超過.'
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro IterablePatternLengthLimitException}
    IterablePatternLengthLimitException();

}

/// {@template ForbidSetException}
/// old BpriverOriginExtensionExceptionC.
/// 列挙した 文字列 が指定される(※空文字は許容する).
/// {@endtemplate}
final class ForbidSetException
    extends
        BpriverOriginExtensionException
{

    static const LOGGER_RESULT_MESSAGE = const [
        '列挙した 文字列 が指定される(※空文字は許容する)',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro ForbidSetException}
    const ForbidSetException();

}

/// {@template OnlyOneCharacterException}
/// old BpriverOriginExtensionExceptionD.
/// 文字数が1以外.
/// {@endtemplate}
final class OnlyOneCharacterException
    extends
        BpriverOriginExtensionException
{

    static const LOGGER_RESULT_MESSAGE = const [
        '文字数が1以外',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro OnlyOneCharacterException}
    const OnlyOneCharacterException();

}

/// {@template LowerLimitException}
/// old BpriverOriginExtensionExceptionE.
/// value is too small.
/// {@endtemplate}
final class LowerLimitException
    extends
        BpriverOriginExtensionException
{

    static const LOGGER_RESULT_MESSAGE = const [
        'value is too small',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro LowerLimitException}
    const LowerLimitException();

}

/// {@template NotEmptyException}
/// value is empty.
/// {@endtemplate}
class NotEmptyException
    extends
        BpriverOriginExtensionException
{

    static const LOGGER_RESULT_MESSAGE = [
        'value is empty',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro NotEmptyException}
    NotEmptyException();

}
