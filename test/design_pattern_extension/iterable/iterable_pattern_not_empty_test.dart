// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class IterablePatternNotEmptyMock
    extends
        Computation
    with
        ListPattern<Object>
        ,IterablePatternNotEmpty<Object>
{

    IterablePatternNotEmptyMock(this.values) {
        checkIterablePatternNotEmpty();
    }
    
    @override
    final Iterable<Object> values;

}

void main() {

    group('checkIterablePatternNotEmpty', () {
        test('expected: return void.', () {
            final base = IterablePatternNotEmptyMock(['a']);
            final actual = base;
            final expected = isA<IterablePatternNotEmptyMock>();
            expect(actual, expected);
        });
        test('exception: throw IterablePatternNotEmptyException', () {
            final base = () => IterablePatternNotEmptyMock([]);
            final actual = base;
            final expected = throwsA(TypeMatcher<Failure<Object, IterablePatternNotEmptyException>>());
            expect(actual, expected);
        });
    });

}
