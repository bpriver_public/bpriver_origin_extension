// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class IterablePatternLengthLimitMock
    extends
        Computation
    with
        ListPattern<Object>
        ,IterablePatternLengthLimit<Object>
{
    
    IterablePatternLengthLimitMock(this.values) {
        checkIterablePatternLengthLimit();
    }

    final Iterable<Object> values;
    
    @override
    int get lengthLimit => 1;
    
}

void main() {

    group('checkIterablePatternLengthLimit', () {
        test('expected: return void', () {
            final target = IterablePatternLengthLimitMock([
                'a',
            ]);
            final result = isA<IterablePatternLengthLimitMock>();
            expect(target, result);
        });
        test('exception: return IterablePatternLengthLimitException', () {
            final target = () => IterablePatternLengthLimitMock([
                'a',
                'b',
            ]);
            final result = throwsA(TypeMatcher<Failure<Object, IterablePatternLengthLimitException>>());
            expect(target, result);
        });
    });

}
