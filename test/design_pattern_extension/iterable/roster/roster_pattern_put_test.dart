// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class EntityPatternMock
    with
        AggregationPattern
        ,EntityPattern<String>
{
    @override
    final String primaryKey;
    final String other;

    EntityPatternMock(this.primaryKey, this.other);

    @override
    Map<String, Object> toMap() {
        return {
            'primaryKey': primaryKey,
            'other': other,
        };
    }

}

class RosterPatternPutMock
    extends
        Computation
    with
        RosterPattern<String, EntityPatternMock>
        ,IterablePatternInternalFactory<EntityPatternMock, Safety<RosterPatternPutMock>>
        ,RosterPatternPut<String, EntityPatternMock, Safety<RosterPatternPutMock>>
{

    @override
    final Iterable<EntityPatternMock> values;
    
    RosterPatternPutMock(this.values);

    static Safety<RosterPatternPutMock> result(Iterable<EntityPatternMock> values) {
        return Safety(RosterPatternPutMock(values));
    }

    Safety<RosterPatternPutMock> internalFactory(Iterable<EntityPatternMock> values) => result(values);

}


void main() {

    final valueA = EntityPatternMock('a', 'aaa');
    final valueB = EntityPatternMock('b', 'bbb');
    final valueC = EntityPatternMock('c', 'ccc');

    final values = [valueA, valueB, valueC];

    group('put', () {
        test('expected: return RosterPatternPutMock then already exists duplicate primary key', () {
            final value = EntityPatternMock('a', 'ddd');
            final base = RosterPatternPutMock(values);
            final result = base.put(value);
            final actual = result.wrapped;
            final expected = RosterPatternPutMock([
                value,
                valueB,
                valueC,
            ]);
            expect(actual, expected);
        });
        test('expected: return RosterPatternPutMock then not exists duplicate primary key', () {
            final value = EntityPatternMock('d', 'ddd');
            final base = RosterPatternPutMock(values);
            final result = base.put(value);
            final actual = result.wrapped;
            final expected = RosterPatternPutMock([
                valueA,
                valueB,
                valueC,
                value,
            ]);
            expect(actual, expected);
        });
    });

}
