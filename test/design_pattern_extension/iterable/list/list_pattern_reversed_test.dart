// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.
    
import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

// class ListPatternReversedMock
//     extends
//         Computation
//     with
//         ListPattern<Object>
//         ,IterablePatternInternalFactory<
//             Object
//             ,ListPatternReversedMock
//         >
//         ,ListPatternReversed<
//             Object
//             ,ListPatternReversedMock
//         >
// {
//     ListPatternReversedMock(this.values);
//     final Iterable<Object> values;
//     @override
//     ListPatternReversedMock internalFactory(Iterable<Object> values) {
//         return ListPatternReversedMock(values);
//     }
// }

void main() {

    // group('reversed', () {
    //     test('', () {
    //         final base = ListPatternReversedMock([
    //             'a',
    //             'b',
    //             'c',
    //         ]);
    //         final actual = base.reversed;
    //         final expected = ListPatternReversedMock([
    //             'c',
    //             'b',
    //             'a',
    //         ]);
    //         expect(actual, expected);
    //         expect(actual.elementAt(0), expected.elementAt(0));
    //         expect(actual.elementAt(1), expected.elementAt(1));
    //         expect(actual.elementAt(2), expected.elementAt(2));
    //     });
    // });

}
