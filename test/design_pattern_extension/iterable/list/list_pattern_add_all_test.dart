// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

// class ListPatternAddAllMock
//     extends
//         Computation
//     with
//         ListPattern
//         ,IterablePatternInternalFactory<
//             Object
//             ,ListPatternAddAllMock
//         >
//         ,ListPatternAddAll<
//             Object
//             ,ListPatternAddAllMock
//         >
// {
//     ListPatternAddAllMock(this.values);
//     final Iterable<Object> values;
//     @override
//     ListPatternAddAllMock internalFactory(Iterable<Object> values) {
//         return ListPatternAddAllMock(values);
//     }
// }


void main() {

    // group('addAll()', () {
    //     test('', () {
    //         final base = ListPatternAddAllMock([
    //             'a',
    //             'b',
    //             'c',
    //         ]);
    //         final actual = base.addAll([
    //             'd',
    //             'e',
    //             'f',
    //         ]);
    //         final expected = ListPatternAddAllMock([
    //             'a',
    //             'b',
    //             'c',
    //             'd',
    //             'e',
    //             'f',
    //         ]);
    //         expect(actual, expected);
    //     });
    // });

}
