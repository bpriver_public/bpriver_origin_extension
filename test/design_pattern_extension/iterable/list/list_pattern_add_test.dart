// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

// class ListPatternAddMock
//     extends
//         Computation
//     with
//         ListPattern
//         ,IterablePatternInternalFactory<
//             Object
//             ,ListPatternAddMock
//         >
//         ,ListPatternAdd<
//             Object
//             ,ListPatternAddMock
//         >
// {
//     ListPatternAddMock(this.values);
//     final Iterable<Object> values;
//     @override
//     ListPatternAddMock internalFactory(Iterable<Object> values) {
//         return ListPatternAddMock(values);
//     }
// }


void main() {

    // group('add()', () {
    //     test('', () {
    //         final base = ListPatternAddMock([
    //             'a',
    //             'b',
    //             'c',
    //         ]);
    //         final actual = base.add('d');
    //         final expected = ListPatternAddMock([
    //             'a',
    //             'b',
    //             'c',
    //             'd',
    //         ]);
    //         expect(actual, expected);
    //     });
    // });

}
