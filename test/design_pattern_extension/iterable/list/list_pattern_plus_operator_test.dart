// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

// class ListPatternPlusOperatorMock
//     extends
//         Computation
//     with
//         ListPattern
//         ,IterablePatternInternalFactory<
//             Object
//             ,ListPatternPlusOperatorMock
//         >
//         ,ListPatternAddAll<
//             Object
//             ,ListPatternPlusOperatorMock
//         >
//         ,ListPatternPlusOperator<
//             Object
//             ,ListPatternPlusOperatorMock
//         >
// {
//     ListPatternPlusOperatorMock(this.values);
//     final Iterable<Object> values;
//     @override
//     ListPatternPlusOperatorMock internalFactory(Iterable<Object> values) {
//         return ListPatternPlusOperatorMock(values);
//     }
// }


void main() {

    // group('operator +', () {
    //     test('', () {
    //         final base = ListPatternPlusOperatorMock([
    //             'a',
    //             'b',
    //             'c',
    //         ]);
    //         final base2 = ListPatternPlusOperatorMock([
    //             'd',
    //             'e',
    //             'f',
    //         ]);
    //         final actual = base + base2;
    //         final expected = ListPatternPlusOperatorMock([
    //             'a',
    //             'b',
    //             'c',
    //             'd',
    //             'e',
    //             'f',
    //         ]);
    //         expect(actual, expected);
    //     });
    // });

}
