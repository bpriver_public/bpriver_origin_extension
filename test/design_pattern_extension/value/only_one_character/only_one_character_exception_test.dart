// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class OnlyOneCharacterMock
    with
        ValuePattern<String>
        ,OnlyOneCharacter<String>
{

    OnlyOneCharacterMock(this.value) {
        checkOnlyOneCharacter();
    }
    
    @override
    final String value;

}

void main() {
    
    group('check()', () {
        test('value is a then ok', () {
            final base = OnlyOneCharacterMock('a');
            final actual = base.value;
            final expected = 'a';
            expect(actual, expected);
        });
        test('value is あ then ok', () {
            final base = OnlyOneCharacterMock('あ');
            final actual = base.value;
            final expected = 'あ';
            expect(actual, expected);
        });
        test('value is empty then failure', () {
            final base = () => OnlyOneCharacterMock('');
            final actual = base;
            final expected = throwsA(TypeMatcher<Failure<Object, OnlyOneCharacterException>>());
            expect(actual, expected);
        });
        test('failure: throw OnlyOneCharacterException then value length is greater then two length', () {
            final base = () => OnlyOneCharacterMock('aa');
            final actual = base;
            final expected = throwsA(TypeMatcher<Failure<Object, OnlyOneCharacterException>>());
            expect(actual, expected);
        });
    });

}
