// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class ForbidStartingCharacterSetMock
    with
        ValuePattern<String>
        ,ForbidStartingCharacterSet
{

    @override
    Set<String> get forbidStartingCharacterSet => {
        'x',
        'y',
        'z',
        'abc',
    };

    ForbidStartingCharacterSetMock(this.value) {
        checkForbidStartingCharacterSet();
    }
    
    @override
    final String value;

}

void main() {
    
    group('check()', () {
        test('value is empty then ok', () {
            final base = ForbidStartingCharacterSetMock('');
            final actual = base;
            final expected = isA<ForbidStartingCharacterSetMock>();
            expect(actual, expected);
        });
        test('value started is forbid x then ForbidStartingCharacterSetExceptionA', () {
            final actual = () => ForbidStartingCharacterSetMock('xabc');
            final expected = throwsA(TypeMatcher<ForbidStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value started is forbid y then ForbidStartingCharacterSetExceptionA', () {
            final actual = () => ForbidStartingCharacterSetMock('yabc');
            final expected = throwsA(TypeMatcher<ForbidStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value started is forbid z then ForbidStartingCharacterSetExceptionA', () {
            final actual = () => ForbidStartingCharacterSetMock('zabc');
            final expected = throwsA(TypeMatcher<ForbidStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value started is forbid abc then ForbidStartingCharacterSetExceptionA', () {
            final actual = () => ForbidStartingCharacterSetMock('abcxxx');
            final expected = throwsA(TypeMatcher<ForbidStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value started is not forbid a then ok', () {
            final base = ForbidStartingCharacterSetMock('abxyz');
            final actual = base;
            final expected = ForbidStartingCharacterSetMock('abxyz');
            expect(actual, expected);
        });
        test('check error message', () {
            try {
                ForbidStartingCharacterSetMock('x');
            } on ForbidStartingCharacterSetExceptionA catch (e) {
                final actual = e.loggerResultMessage;
                final expected = [
                    '列挙した 文字 で開始.',
                    "{x, y, z, abc} 以外の文字で開始してください.",
                    'value の値 = x',
                ];
                expect(actual, expected);                
            }
        });
    });

}



