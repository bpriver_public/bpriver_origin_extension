// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class StringValuePatternMock
    with
        ValuePattern<String>
        ,StringValuePatternIsEmpty
{
    StringValuePatternMock(this.value);
    @override
    final String value;
}

void main() {

    group('get isEmpty', () {
        test('if value != empty then false', () {
            final target = StringValuePatternMock('x').isEmpty;
            final result = false;
            expect(target, result);
        });
        test('if value = 0 then false', () {
            final target = StringValuePatternMock('0').isEmpty;
            final result = false;
            expect(target, result);
        });
        test('if value = empty then true', () {
            final target = StringValuePatternMock('').isEmpty;
            final result = true;
            expect(target, result);
        });
    });

}