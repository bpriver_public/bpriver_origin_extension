// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class AllowStartingCharacterSetMock
    with
        ValuePattern<String>
        ,AllowStartingCharacterSet
{
    @override
    final String value;
    AllowStartingCharacterSetMock(this.value) {
        checkAllowStartingCharacterSet();
    }

    @override
    Set<String> get allowStartingCharacterSet => const {
        'a',
        'b',
        'c',
    };

}

void main() {
    
    group('check()', () {
        test('value is empty then ok', () {
            final base = AllowStartingCharacterSetMock('');
            final actual = base;
            final expected = isA<AllowStartingCharacterSetMock>();
            expect(actual, expected);
        });
        test('starting value is listed value a then ok', () {
            final actual = AllowStartingCharacterSetMock('a');
            final expected = isA<AllowStartingCharacterSetMock>();
            expect(actual, expected);
        });
        test('starting value is listed value b then ok', () {
            final actual = AllowStartingCharacterSetMock('bdd');
            final expected = isA<AllowStartingCharacterSetMock>();
            expect(actual, expected);
        });
        test('starting value is listed value c then ok', () {
            final actual = AllowStartingCharacterSetMock('ca');
            final expected = isA<AllowStartingCharacterSetMock>();
            expect(actual, expected);
        });
        test('starting value is not listed value x then throw AllowStartingCharacterSetExceptionA', () {
            final actual = () => AllowStartingCharacterSetMock('x');
            final expected = throwsA(TypeMatcher<AllowStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('starting value is not listed value A then throw AllowStartingCharacterSetExceptionA', () {
            final actual = () => AllowStartingCharacterSetMock('A');
            final expected = throwsA(TypeMatcher<AllowStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('starting value is not listed value 0 then throw AllowStartingCharacterSetExceptionA', () {
            final actual = () => AllowStartingCharacterSetMock('0');
            final expected = throwsA(TypeMatcher<AllowStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('starting value is not listed value あ then throw AllowStartingCharacterSetExceptionA', () {
            final actual = () => AllowStartingCharacterSetMock('あ');
            final expected = throwsA(TypeMatcher<AllowStartingCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('check AllowStartingCharacterSetExceptionA message', () {
            try {
                AllowStartingCharacterSetMock('あ');
            } on AllowStartingCharacterSetExceptionA catch (e) {
                final actual = e.loggerResultMessage;
                final expected = [
                    '列挙した 文字 以外で開始.',
                    "{a, b, c} の文字で開始してください.",
                    'value の値 = あ',
                ];
                expect(actual, expected);                
            }
        });
    });

}



