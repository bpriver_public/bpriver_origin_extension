// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:test/test.dart';

import 'lower_case_alphabet_mock.dart';

void main() { group('LowerCaseAlphabetTypeAnnotationCase', () {
    
    group('get typeAnnotationCase', () {
        test('if value = num then return num', () {
            final target =
                LowerCaseAlphabetMock('num')
            ;
            final result =
                'num'
            ;
            expect(target.typeAnnotationCase, result);
        });
        test('if value = int then return int', () {
            final target =
                LowerCaseAlphabetMock('int')
            ;
            final result =
                'int'
            ;
            expect(target.typeAnnotationCase, result);
        });
        test('if value = int then return double', () {
            final target =
                LowerCaseAlphabetMock('double')
            ;
            final result =
                'double'
            ;
            expect(target.typeAnnotationCase, result);
        });
        test('if value = bool then return bool', () {
            final target =
                LowerCaseAlphabetMock('bool')
            ;
            final result =
                'bool'
            ;
            expect(target.typeAnnotationCase, result);
        });
        test('if value = void then return void', () {
            final target =
                LowerCaseAlphabetMock('void')
            ;
            final result =
                'void'
            ;
            expect(target.typeAnnotationCase, result);
        });
        test('if value = dynamic then return dynamic', () {
            final target =
                LowerCaseAlphabetMock('dynamic')
            ;
            final result =
                'dynamic'
            ;
            expect(target.typeAnnotationCase, result);
        });
        test('if value = string then return String', () {
            final target =
                LowerCaseAlphabetMock('string')
            ;
            final result =
                'String'
            ;
            expect(target.typeAnnotationCase, result);
        });
    });

});}



