// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:test/test.dart';

import 'lower_case_alphabet_mock.dart';

void main() { group('LowerCaseAlphabetPascalCase', () {
    
    group('get pascalCase', () {
        final target =
            LowerCaseAlphabetMock('actor name')
        ;
        final result =
            'ActorName'
        ;
        test('', () {
            expect(target.pascalCase, result);
        });
    });

});}



