// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

import 'lower_case_alphabet_mock.dart';

void main() { group('LowerCaseAlphabetErrorA', () {

    group('validationLowerCaseAlphabet()', () {
        test('if not lower case then throw LowerCaseAlphabetErrorA', () {
            expect(() => LowerCaseAlphabetMock('Abc'), throwsA(TypeMatcher<LowerCaseAlphabetExceptionA>()));
            expect(() => LowerCaseAlphabetMock('a B c'), throwsA(TypeMatcher<LowerCaseAlphabetExceptionA>()));
            // expect(() => LowerCaseAlphabetMock('a_b'), throwsA(TypeMatcher<LowerCaseAlphabetExceptionA>()));
            // final ab = LowerCaseAlphabetMock('a_b');
            // final a = LowerCaseAlphabetMock('あ');
            // print(a.camelCase);
        });
        test('if empty then ok', () {
            expect(LowerCaseAlphabetMock(''), LowerCaseAlphabetMock(''));
        });
    });

});}
