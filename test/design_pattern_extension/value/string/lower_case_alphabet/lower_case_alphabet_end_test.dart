// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class LowerCaseAlphabetMock
    with
        ValuePattern<String>
        ,LowerCaseAlphabet
        ,LowerCaseAlphabetEnd
{
    
    LowerCaseAlphabetMock(this.value) {
        checkLowerCaseAlphabet();
    }
    
    @override
    final String value;

}

void main() {
    
    group('get end', () {
        final target = LowerCaseAlphabetMock('a b c').end;
        final result = 'c';
        test('', () {
            expect(target, result);
        });
    });

}



