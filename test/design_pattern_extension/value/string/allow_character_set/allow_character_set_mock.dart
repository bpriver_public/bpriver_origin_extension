// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

class AllowCharacterSetMock
    with
        ValuePattern<String>
        ,AllowCharacterSet
{

    @override
    Set<String> get allowCharacterSet => {
        'x',
        'y',
        'z',
    };

    AllowCharacterSetMock(this.value) {
        checkAllowCharacterSet();
    }
    
    @override
    final String value;

}