// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

import 'allow_character_set_mock.dart';

void main() {
    
    group('check()', () {
        test('if value is empty then allow', () {
            final target = AllowCharacterSetMock('');
            final result = isA<AllowCharacterSetMock>();
            expect(target, result);
        });
        test('if value is allow x then alllow', () {
            final target = AllowCharacterSetMock('x');
            final result = isA<AllowCharacterSetMock>();
            expect(target, result);
        });
        test('if value is allow y then alllow', () {
            final target = AllowCharacterSetMock('y');
            final result = isA<AllowCharacterSetMock>();
            expect(target, result);
        });
        test('if value is allow z then alllow', () {
            final target = AllowCharacterSetMock('z');
            final result = isA<AllowCharacterSetMock>();
            expect(target, result);
        });
        test('if value type is not allow then error', () {
            final target = () => AllowCharacterSetMock('a');
            final result = throwsA(TypeMatcher<AllowCharacterSetExceptionA>());
            expect(target, result);
        });
        test('check error message', () {
            try {
                AllowCharacterSetMock('a');
            } on AllowCharacterSetExceptionA catch (e) {
                print(e);
            }
        });
    });

}



