// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class ForbidCharacterSetMock
    with
        ValuePattern<String>
        ,ForbidCharacterSet
{

    @override
    Set<String> get forbidCharacterSet => {
        'x',
        'y',
        'z',
        '==',
        'abc',
    };

    ForbidCharacterSetMock(this.value) {
        checkForbidCharacterSet();
    }
    
    @override
    final String value;

}

void main() {
    
    group('check()', () {
        test('value is empty then ok', () {
            final base = ForbidCharacterSetMock('');
            final actual = base;
            final expected = isA<ForbidCharacterSetMock>();
            expect(actual, expected);
        });
        test('value is forbid x then ForbidCharacterSetExceptionA', () {
            final actual = () => ForbidCharacterSetMock('ax');
            final expected = throwsA(TypeMatcher<ForbidCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value is forbid y then ForbidCharacterSetExceptionA', () {
            final actual = () => ForbidCharacterSetMock('yb');
            final expected = throwsA(TypeMatcher<ForbidCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value is forbid z then ForbidCharacterSetExceptionA', () {
            final actual = () => ForbidCharacterSetMock('czc');
            final expected = throwsA(TypeMatcher<ForbidCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value is forbid == then ForbidCharacterSetExceptionA', () {
            final base = () => ForbidCharacterSetMock('==');
            final actual = base;
            final expected = throwsA(TypeMatcher<ForbidCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value is forbid abc then ForbidCharacterSetExceptionA', () {
            final base = () => ForbidCharacterSetMock('abc');
            final actual = base;
            final expected = throwsA(TypeMatcher<ForbidCharacterSetExceptionA>());
            expect(actual, expected);
        });
        test('value is not forbid a,b,c,=, then ok', () {
            final base = ForbidCharacterSetMock('a=b=c');
            final actual = base;
            final expected = ForbidCharacterSetMock('a=b=c');
            expect(actual, expected);
        });
        test('value is not forbid a then ok', () {
            final base = ForbidCharacterSetMock('abdc');
            final actual = base;
            final expected = ForbidCharacterSetMock('abdc');
            expect(actual, expected);
        });
        test('check error message', () {
            try {
                ForbidCharacterSetMock('x');
            } on ForbidCharacterSetExceptionA catch (e) {
                final actual = e.loggerResultMessage;
                final expected = [
                    '列挙した 文字 を利用.',
                    "{x, y, z, ==, abc} の文字は使用しないでください.",
                    'value の値 = x',
                ];
                expect(actual, expected);                
            }
        });
    });

}
