// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class ForbidSetMock
    with
        ValuePattern<String>
        ,ForbidSet
{
    @override
    final String value;
    ForbidSetMock(this.value) {
        checkForbidSet();
    }
    @override
    Set<String> get forbidSet => {
        'a',
        'b',
        'c',
    };
}

void main() {
    
    group('check()', () {
        test('value is empty then ok', () {
            final base = ForbidSetMock('');
            final actual = base.value;
            final expected = '';
            expect(actual, expected);
        });
        test('value is a then ForbidSetException', () {
            final actual = () => ForbidSetMock('a');
            final expected = throwsA(TypeMatcher<Failure<Object, ForbidSetException>>());
            expect(actual, expected);
        });
        test('value is b then ForbidSetException', () {
            final actual = () => ForbidSetMock('b');
            final expected = throwsA(TypeMatcher<Failure<Object, ForbidSetException>>());
            expect(actual, expected);
        });
        test('value is c then ForbidSetException', () {
            final actual = () => ForbidSetMock('c');
            final expected = throwsA(TypeMatcher<Failure<Object, ForbidSetException>>());
            expect(actual, expected);
        });
        test('value is abc then ok', () {
            final base = ForbidSetMock('abc');
            final actual = base.value;
            final expected = 'abc';
            expect(actual, expected);
        });
    });


}
