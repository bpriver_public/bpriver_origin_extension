// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

import 'allow_set_mock.dart';

void main() {
    
    group('static check()', () {
        test('if value = a then error', () {
            expect(() => AllowSetMock('a'), throwsA(TypeMatcher<AllowSetExceptionA>()));
        });
        test('if value = x or y or z then ok', () {
            expect(AllowSetMock('x'), isA<AllowSetMock>());
            expect(AllowSetMock('y'), isA<AllowSetMock>());
            expect(AllowSetMock('z'), isA<AllowSetMock>());
        });
        test('if value is empty then ok', () {
            expect(AllowSetMock(''), isA<AllowSetMock>());
        });
    });

}



