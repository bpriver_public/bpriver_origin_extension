// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class IdentifierMock
    with
        ValuePattern<String>
        ,Identifier
{
    IdentifierMock(this.value) {
        checkIdentifier();
    }
    @override
    final String value;
}

void main(){

    final rejection_lessThan = 20;
    final allow = 21;
    final rejection_moreThan = 22;

    group('validation()', () {
        test('文字数が 21 以外だと IdentifierExceptionA を throw',(){
            var lessThan = '01234567890123456789'; // 20文字 => error
            expect(lessThan.length,equals(rejection_lessThan));
            expect( ()=> IdentifierMock(lessThan), throwsA(TypeMatcher<IdentifierExceptionA>()) );

            var moreThan = '0123456789012345678901'; // 22文字 => error
            expect(moreThan.length,equals(rejection_moreThan));
            expect( ()=> IdentifierMock(moreThan), throwsA(TypeMatcher<IdentifierExceptionA>()) );
        });
        test('利用文字が指定外のものだと IdentifierExceptionB を throw',(){
            var exclamation = '01234567890123456789!'; // 21文字 => ok
            expect(exclamation.length,equals(allow));
            expect( ()=> IdentifierMock(exclamation), throwsA(TypeMatcher<IdentifierExceptionB>()) );
        });
        test('allow', (){
            var idBase = '012345678901234567890';
            expect(idBase.length,equals(allow));
            var id = IdentifierMock(idBase);
            expect( id, isA<IdentifierMock>() );
            expect( id.toString(), equals(idBase));
        });
        test('if value is empty then allow', (){
            final target = IdentifierMock('');
            final result = isA<IdentifierMock>();
            expect(target, result);
        });
    });

    group('static generatID()', () {
        final a = IdentifierMock(Identifier.generateID());
        final b = IdentifierMock(Identifier.generateID());
        final c = IdentifierMock(Identifier.generateID());
        test('allow', () {
            expect(a, isA<IdentifierMock>());
            expect(b, isA<IdentifierMock>());
            expect(c, isA<IdentifierMock>());
        });
    });

    group('inactivate()', () {
        test('', () {
            final target = IdentifierMock(Identifier.generateID()).inactivate();
            final result = 'xxxxxxxxxxxxxxxxxxxxx';
            expect(target, result);
        });
        test('', () {
        final target = IdentifierMock(
            IdentifierMock(Identifier.generateID()).inactivate()
        );
        final result = isA<IdentifierMock>();
            expect(target, result);
        });
    });

}
