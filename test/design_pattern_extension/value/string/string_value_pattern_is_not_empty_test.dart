// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class StringValuePatternMock
    with
        ValuePattern<String>
        ,StringValuePatternIsNotEmpty
{
    StringValuePatternMock(this.value);
    @override
    final String value;
}

void main() {

    group('get isNotEmpty', () {
        test('value is not empty then true', () {
            final target = StringValuePatternMock('x').isNotEmpty;
            final result = true;
            expect(target, result);
        });
        test('value is 0 then ture', () {
            final target = StringValuePatternMock('0').isNotEmpty;
            final result = true;
            expect(target, result);
        });
        test('value is empty then false', () {
            final target = StringValuePatternMock('').isNotEmpty;
            final result = false;
            expect(target, result);
        });
    });

}