// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class TypeValuePatternAllowSetMock
    with
        ValuePattern<Type>
        ,TypeValuePatternAllowSet
{
    
    TypeValuePatternAllowSetMock(this.value) {
        checkAllowSet();
    }

    final Type value;
    
    @override
    Set<Type> get allowSet => {
        String,
        int,
    };

}

void main() {

    group('check()', () {
        test('if value type is allow then', () {
            final target = TypeValuePatternAllowSetMock(String);
            final result = isA<TypeValuePatternAllowSetMock>();
            expect(target, result);
        });
        test('if value type is allow then 2', () {
            final target = TypeValuePatternAllowSetMock(int);
            final result = isA<TypeValuePatternAllowSetMock>();
            expect(target, result);
        });
        test('if value type is not allow then error', () {
            final target = () => TypeValuePatternAllowSetMock(num);
            final result = throwsA(TypeMatcher<TypeValuePatternAllowSetExceptionA>());
            expect(target, result);
        });
        test('if value type is not allow then error 2', () {
            final target = () => TypeValuePatternAllowSetMock(double);
            final result = throwsA(TypeMatcher<TypeValuePatternAllowSetExceptionA>());
            expect(target, result);
        });
        test('if value type is not allow then error 3', () {
            final target = () => TypeValuePatternAllowSetMock(Object);
            final result = throwsA(TypeMatcher<TypeValuePatternAllowSetExceptionA>());
            expect(target, result);
        });
        test('check error message', () {
            try {
                TypeValuePatternAllowSetMock(Object);
            } on TypeValuePatternAllowSetExceptionA catch (e) {
                print(e);
            }
        });
    });

}
