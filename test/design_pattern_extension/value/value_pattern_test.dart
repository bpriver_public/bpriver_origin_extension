// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:convert';

import 'package:test/test.dart';
import 'package:bpriver_origin/bpriver_origin.dart';

class ValuePatternMock
    with
        ValuePattern<String>
{
    ValuePatternMock(this.value);
    @override
    final String value;
    String inactivate() {
        return 'xxx';
    }
}

/// == operator test 用。値が同じでも型が異なれば false を返すか test.
class ValuePatternMock2
    with
        ValuePattern<String>
{
    ValuePatternMock2(this.value);
    @override
    final String value;
    String inactivate() {
        return 'xxx';
    }
}

class ValuePatternMock3
    with
        ValuePattern<ValuePatternMock>
{
    ValuePatternMock3(this.value);
    @override
    final ValuePatternMock value;
}

class EnumPatternMockA with EnumPattern { final value = 'a'; }

class ValuePatternMock4
    with
        ValuePattern<EnumPatternMockA>
{
    ValuePatternMock4(this.value);
    @override
    final EnumPatternMockA value;
}

void main(){

    final valueABase = 'hello A';
    final valueBBase = 'hello B';

    final valueA = ValuePatternMock(valueABase);
    final valueB = ValuePatternMock(valueBBase);

    /// hashCode 参考。
    /// https://engineer-club.jp/java-hashcode#hashCode-3
    /// 
    /// ・equalsでの判断で使うフィールドが変わらないなら、hashCodeの値も変わらない
    /// ・equalsの結果がtrueなら、違うインスタンスでもhashCodeの値は同じになる
    /// ・equalsの結果がfalseなら、hashCodeの値は違わなくてもいい
    /// 
    group('get hashCode', () {
        final otherInstance  = ValuePatternMock2(valueABase);
        final otherInstance2 = ValuePatternMock2(valueABase);
        test('value が同じなら、hashCode の値も変わらない', () {
            expect(valueA.value, equals(otherInstance.value));
            expect(otherInstance.hashCode, equals(otherInstance2.hashCode));
        });
        test('not equals', () {
            expect(valueA.hashCode == valueB.hashCode, equals(false));
        });
    });

    group('operator ==', () {
        test('true', () {
            expect(valueA, valueA);
        });
        test('not equals value => false', () {
            expect(valueA == valueB, false);
        });
        test('not equals type => false', () {
            final sameType = ValuePatternMock (valueABase);
            final otherType    = ValuePatternMock2(valueABase);
            expect(sameType.toString(), equals(otherType.toString()));
            expect(sameType == otherType, false);
        });
    });

    group('toString()', () {
        test('equals', () {
            expect(valueA.toString(), equals(valueABase));
        });
    });

    group('inactivate()', () {
        test('', () {
            final target = ValuePatternMock('a').inactivate();
            final result = 'xxx';
            expect(target, result);
        });
    });

    group('toMap()', () {
        test('expected: return Map', () {
            final base = ValuePatternMock('a');
            final actual = base.toMap();
            final expected = {
                'value': 'a',
            };
            expect(actual, expected);
        });
        test('expected: return Map then EnumPattern', () {
            final base = ValuePatternMock4(EnumPatternMockA());
            final actual = base.toMap();
            final expected = {
                'value': 'a',
            };
            expect(actual, expected);
        });
    });

    group('toJson()', () {
        test('then vlaue type is BuiltInType', () {
            final base = ValuePatternMock('a');
            final actual = base.toJson();
            final expected = {
                'value': 'a'
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then vlaue type is ValuePattern', () {
            final base = ValuePatternMock3(
                ValuePatternMock('a'),
            );
            final actual = base.toJson();
            final expected = {
                'value': {
                    'value': 'a',
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
    });

}