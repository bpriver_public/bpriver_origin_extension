// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

/// -100 から 100 までの 整数（int 型）
class BetweenXAndYMock
    with
        ValuePattern<int>
        ,BetweenXAndY<int>
{
    BetweenXAndYMock(this.value){
        checkBetweenXAndY();
    }

    @override
    int get lowerLimit => -100;

    @override
    int get upperLimit => 100;

    @override
    final int value;
}

void main(){

    final lowerLimit = -100;
    final upperLimit = 100;
    final valueA = lowerLimit - 1;
    final valueB = lowerLimit;
    final valueC = 0;
    final valueD = upperLimit;
    final valueE = upperLimit + 1;

    // final org = BetweenXAndYMock(10);
    // final same = BetweenXAndYMock(10);
    // final small = BetweenXAndYMock(9);
    // final big = BetweenXAndYMock(11);

    group('validation()', () {
        test('validity then $lowerLimit <= value <= $upperLimit', () {
            expect(BetweenXAndYMock(valueB).value, valueB);
            expect(BetweenXAndYMock(valueC).value, valueC);
            expect(BetweenXAndYMock(valueD).value, valueD);
        });
        test('throw error then value < $lowerLimit', () {
            expect(() => BetweenXAndYMock(valueA), throwsA(TypeMatcher<BetweenXAndYExceptionA>()));
        });
        // test('throw error message then value < $lowerLimit', () {
        //     final productError = BetweenXAndYExceptionA(
        //         BetweenXAndYMock,
        //         BetweenXAndY,
        //         [
        //             '入力された値 = 「$valueA」',
        //             '値が小さすぎます。',
        //             '利用できるのは、「$lowerLimit ～ $upperLimit」の、$T 型の値です。'
        //         ]
        //     );
        //     try {
        //         BetweenXAndYMock(valueA);
        //     } on ProductError catch (_) {
        //         expect(_,productError);
        //     }
        // });
        test('throw error then value > $upperLimit', () {
            expect(() => BetweenXAndYMock(valueE), throwsA(TypeMatcher<BetweenXAndYExceptionB>()));
        });
        // test('throw error message then value > $upperLimit', () {
        //     final productError = ProductError(
        //         BetweenXAndYMock,
        //         BetweenXAndY,
        //         [
        //             '入力された値 = 「$valueE」',
        //             '値が大きすぎます。',
        //             '利用できるのは、「$lowerLimit ～ $upperLimit」の、$T 型の値です。'
        //         ]
        //     );
        //     try {
        //         BetweenXAndYMock(valueE);
        //     } on ProductError catch (_) {
        //         expect(_,productError);
        //     }
        // });
    });

}