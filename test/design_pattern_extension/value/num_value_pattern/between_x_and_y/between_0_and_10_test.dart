// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class Mock 
    with
        ValuePattern<num>
        ,BetweenXAndY<
            num
        >
        ,Between0And10<
            num
        >
{
    Mock(this.value){
        checkBetweenXAndY();
    }

    @override
    final num value;
}
void main(){

    final lowerLimit = 0;
    final upperLimit = 10;
    final lowerLimitOver = lowerLimit - 1;
    final upperLimitOver = upperLimit + 1;

    group('validation()', () {
        test('ok', () {
            expect(Mock(lowerLimit), TypeMatcher<Mock>());
            expect(Mock(upperLimit), TypeMatcher<Mock>());
        });
        test('error', () {
            expect(()=>Mock(lowerLimitOver), throwsA(TypeMatcher<BetweenXAndYExceptionA>()));
            expect(()=>Mock(upperLimitOver), throwsA(TypeMatcher<BetweenXAndYExceptionB>()));
        });
    });

}