// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

abstract class Base<
        MS extends Base<MS>
    >
    with
        ValuePattern<int>
        ,BetweenXAndY<
            int
        >
        ,BetweenMinus100And100<
            int
        >
        ,BetweenXAndYOperator<
            int
            ,MS
        >
{
    


    @override
    final int value;

    Base(this.value);

}

class Mock
    extends
        Base<
            Mock
        >
{

  Mock(int value) : super(value);

  @override
  Mock internalFactory(int value) => Mock(value);

  
}

class Mock2
    extends
        Base<
            Mock2
        >
{
  Mock2(int value) : super(value);
  @override
  Mock2 internalFactory(int value) => Mock2(value);
}

class Mock3
    extends
        Base<
            Mock3
        >
{
  Mock3(int value) : super(value);
  @override
  Mock3 internalFactory(int value) => Mock3(value);
}

void main(){

    const lowerLimit = -100;
    const upperLimit = 100;

    final value10 = 10;
    final value9 = 9;
    final value0 = 0;
    final value5 = 5;
    final value90 = 90;
    final value20 = 20;

    group('operator + (plus) lowerLimit = $lowerLimit upperLimit = ${upperLimit}', () {
        test('$value10 + $value9 = ${value10 + value9}', () {
            expect(Mock(value10) + Mock(value9), Mock(value10 + value9));
        });
        test('$value0 + $value0 = ${value0 + value0}', () {
            expect(Mock(value0) + Mock(value0), Mock(value0 + value0));
        });
        test('$value5 + -$value10 = -${value5 + -value10}', () {
            expect(Mock(value5) + Mock(-value10), Mock(value5 + -value10));
        });
        test('${upperLimit} + $value10 => ${upperLimit}', () {
            expect(Mock(upperLimit) + Mock(value10), Mock(upperLimit));
        });
        test('-$value90 + -$value20 => ${lowerLimit}', () {
            expect(Mock(-value90) + Mock(-value20), Mock(lowerLimit));
        });
        // test('same base type, Mock $value10 + Mock2 $value9 => ${value10 + value9}', () {
        //     expect(Mock(value10) + Mock2(value9), Mock(value10 + value9));
        // });
        // test('same base type, Mock $value90 + Mock2 $value20 => ${upperLimit}', () {
        //     expect(Mock(value90) + Mock2(value20), Mock(upperLimit));
        // });
        test('other type, type error ※これは型チェック時にエラー表示されればOK。', () {
            // expect(Mock3(value10) + Mock(value9), Mock3(value10 + value9));
        });
    });

    group('operator - (minus) lowerLimit = ${lowerLimit} upperLimit = ${upperLimit}', () {
        test('$value10 - $value9 = ${value10 - value9}', () {
            expect(Mock(value10) - Mock(value9), Mock(value10 - value9));
        });
        test('$value0 - $value0 = ${value0 - value0}', () {
            expect(Mock(value0) - Mock(value0), Mock(value0));
        });
        test('$value5 - -$value10 = -${value5 - -value10}', () {
            expect(Mock(value5) - Mock(-value10), Mock(value5 - -value10));
        });
        test('${lowerLimit} - $value10 => ${lowerLimit}', () {
            expect(Mock(lowerLimit) - Mock(value10), Mock(lowerLimit));
        });
        test('${upperLimit} - -$value10 => ${upperLimit}', () {
            expect(Mock(upperLimit) - Mock(-value10), Mock(upperLimit));
        });
        //  test('same base type, Mock $value10 - Mock2 $value9 => ${value10 - value9}', () {
        //     expect(Mock(value10) - Mock2(value9), Mock(value10 - value9));
        // });
        // test('same base type, Mock -$value90 - Mock2 $value20 => ${lowerLimit}', () {
        //     expect(Mock(-value90) - Mock2(value20), Mock(lowerLimit));
        // });
        test('other type, type error', () {
            // expect(Mock3(value10) - Mock(value9), Mock3(value10 - value9));
        });
    });

}