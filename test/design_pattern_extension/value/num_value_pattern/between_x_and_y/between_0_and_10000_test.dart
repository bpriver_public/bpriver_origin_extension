// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

// ignore_for_file: omit_local_variable_types

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class Mock 
    with
        // DoubleValuePattern が正常に動くか確認
        ValuePattern<double>
        ,BetweenXAndY<
            double
        >
        ,Between0And10000<
            double
        >
{
    Mock(this.value){
        checkBetweenXAndY();
    }

    @override
    final double value;
}

void main(){

    final double lowerLimit = 0;
    final double upperLimit = 10000;
    final double lowerLimitOver = lowerLimit - 1;
    final double upperLimitOver = upperLimit + 1;

    group('validation()', () {
        test('ok', () {
            expect(Mock(lowerLimit), TypeMatcher<Mock>());
            expect(Mock(upperLimit), TypeMatcher<Mock>());
        });
        test('error', () {
            expect(()=>Mock(lowerLimitOver), throwsA(TypeMatcher<BetweenXAndYExceptionA>()));
            expect(()=>Mock(upperLimitOver), throwsA(TypeMatcher<BetweenXAndYExceptionB>()));
        });
    });

}