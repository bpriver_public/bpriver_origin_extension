// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:test/test.dart';

import '../num_value_pattern_mock.dart';

void main(){

    final org = NumValuePatternMock(5);
    final same = NumValuePatternMock(5);
    final small = NumValuePatternMock(4);
    final big = NumValuePatternMock(6);
    group('operator > (more than)', () {
        test('when org > same, false', () {
            expect(org > same, false);
        });
        test('when org > small, false', () {
            expect(org > small, true);
        });
        test('when org > big, true', () {
            expect(org > big, false);
        });
        test('when other type, false', () {
            // 型チェック時に検出されればOK
            // expect(org > otherTypeSmall, false);
        });
    });

    group('operator < (less than)', () {
        test('when org < same, false', () {
            expect(org < same, false);
        });
        test('when org < small, false', () {
            expect(org < small, false);
        });
        test('when org < big, true', () {
            expect(org < big, true);
        });
        test('when other type, false', () {
            // 型チェック時に検出されればOK
            // expect(org > otherTypeSmall, false);
        });
    });


}