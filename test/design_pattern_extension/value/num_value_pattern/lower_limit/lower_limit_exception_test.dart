// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';
import 'package:test/test.dart';

class LowerLimitMock
    with
        ValuePattern<int>
        ,LowerLimit<int>
{
    @override
    final int value;
    LowerLimitMock(this.value){
        checkLowerLimit();
    }
    @override
    int get lowerLimit => 1;
}

void main(){

    group('check', () {
        test('ok then value is 1.', () {
            final base = LowerLimitMock(1);
            final actual = base.value;
            final expected = 1;
            expect(actual, expected);
        });
        test('ok then value is greater than 1.', () {
            final base = LowerLimitMock(2);
            final actual = base.value;
            final expected = 2;
            expect(actual, expected);
        });
        test('throw exception then value is less than 1.', () {
            final base = () => LowerLimitMock(0);
            final actual = base;
            final expected = throwsA(TypeMatcher<Failure<Object, LowerLimitException>>());
            expect(actual, expected);
        });
    });

}