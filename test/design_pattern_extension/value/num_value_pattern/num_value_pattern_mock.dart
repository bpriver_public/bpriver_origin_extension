// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_origin_extension/bpriver_origin_extension.dart';

class NumValuePatternMock
    with
        ValuePattern<num>
        ,BetweenXAndY<
            num
        >
        ,Between0And100<
            num
        >
        ,NumValuePatternThanOperator<num,NumValuePatternMock>
        ,NumValuePatternGetPx<num>
        ,NumValuePatternGetPercent<num>
        ,NumValuePatternGetFr<num>
        ,NumValuePatternGetEm<num>
        ,NumValuePatternGetEx<num>
{
    NumValuePatternMock(this.value);
    @override
    final num value;
}
