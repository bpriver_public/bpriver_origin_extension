// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:test/test.dart';

import '../../num_value_pattern_mock.dart';

void main(){

    final valueA = NumValuePatternMock(1);
    group('get em', () {
        test('equals', () {
            expect(valueA.em, '1em');
        });
    });

}